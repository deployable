remote-at: remote
	./mobundle -PB remote -o remote-at \
	           -m Package::Constants \
	           -m Archive::Tar \
	           -m Archive::Tar::Constant \
	           -m Archive::Tar::File
	chmod +x remote-at

bundle/deployable: deployable remote
	mkdir -p bundle
	./mobundle -PB deployable -o bundle/deployable \
	           -m Text::Glob \
	           -m Number::Compare \
	           -m File::Find::Rule \
	           -m Archive::Tar \
	           -m Archive::Tar::Constant \
	           -m Archive::Tar::File
	cat remote >> bundle/deployable
	chmod +x bundle/deployable

bundled: bundle/deployable
