#!/usr/bin/env perl
use strict;
use warnings;
use Carp;
use version; our $VERSION = qv('0.1.2');
use Fatal qw( close );
use Pod::Usage qw( pod2usage );
use Getopt::Long qw( :config gnu_getopt );
use English qw( -no_match_vars );
use File::Basename qw( basename dirname );
use File::Spec::Functions qw( file_name_is_absolute catfile );
use File::Temp qw( tempfile );
use POSIX qw( strftime );
use Cwd qw( cwd realpath );
use Data::Dumper;
use Encode;

use File::Find::Rule;
use Archive::Tar;

my %config = (
   output      => '-',
   remote      => catfile(dirname(realpath(__FILE__)), 'remote'),
   heredir     => [],
   herefile    => [],
   rootdir     => [],
   root        => [],
   rootfile    => [],
   tarfile     => [],
   deploy      => [],
   xform       => [],
   passthrough => 0,
);
GetOptions(
   \%config,
   qw(
     usage! help! man! version!

     bundle|all-exec|X!
     bzip2|bz2|j!
     cleanup|c!
     deploy|exec|d=s@
     gzip|gz|z!
     heredir|H=s@
     include-archive-tar|T!
     no-tar!
     output|o=s
     passthrough|P!
     root|r=s@
     rootdir|in-root|R=s@
     tar|t=s
     tarfile|F=s@
     tempdir-mode|m=s
     xform|x=s@
     workdir|work-directory|deploy-directory|w=s
     ),
) or pod2usage(message => "invalid command line", -verbose => 99, -sections => ' ', -noperldoc => 1);
pod2usage(message => "$0 $VERSION", -verbose => 99, -sections => ' ', -noperldoc => 1)
  if $config{version};
pod2usage(-verbose => 99, -sections => 'USAGE', -noperldoc => 1) if $config{usage};
pod2usage(-verbose => 99, -sections => 'USAGE|EXAMPLES|OPTIONS', -noperldoc => 1)
  if $config{help};
pod2usage(-verbose => 2, -noperldoc => 1) if $config{man};

pod2usage(
   message   => 'working directory must be an absolute path',
   -verbose  => 99,
   -sections => '',
   -noperldoc => 1
) if exists $config{workdir} && !file_name_is_absolute($config{workdir});

if (@{$config{xform}}) {
   $config{'no-tar'} = 1; # force internal stuff
   for my $xform (@{$config{xform}}) {
      my ($src, $filename) =
         $xform =~ m{\A((?:[^\\:]|\\.)*) : (.*)}mxs;
      s{\\(.)}{$1}gmxs for $src, $filename;
      my $array_name = file_name_is_absolute($filename) ? 'rootfile'
         : 'herefile';
      push @{$config{$array_name}}, [$src, $filename];
   }
}

if ($config{'include-archive-tar'}) {
   $config{remote} = catfile(dirname(realpath(__FILE__)), 'remote-at');
   if (!-e $config{remote}) {    # "make" it
      print {*STDERR} "### Making remote-at...\n";
      my $startdir = cwd();
      chdir dirname realpath __FILE__;
      system {'make'} qw( make remote-at );
      chdir $startdir;
   } ## end if (!-e $config{remote...})
} ## end if ($config{'include-archive-tar'...})

# Establish output channel
my $out_fh = \*STDOUT;
if ($config{output} ne '-') {
   open my $fh, '>', $config{output}    ## no critic
     or croak "open('$config{output}'): $OS_ERROR";
   $out_fh = $fh;
}
binmode $out_fh;

# Emit script code to be executed remotely. It is guaranteed to end
# with __END__, so that all what comes next is data
print {$out_fh} get_remote_script();

# Where all the data will be kept
print_configuration($out_fh, \%config);

print_here_stuff($out_fh, \%config, @ARGV);
print_root_stuff($out_fh, \%config);

close $out_fh;

# Set as executable
if ($config{output} ne '-') {
   chmod oct(755), $config{output}
     or carp "chmod(0755, '$config{output}'): $OS_ERROR";
}

sub header {
   my %params   = @_;
   my $namesize = length $params{name};
   return "$namesize $params{size}\n$params{name}";
}

sub print_configuration {    # FIXME
   my ($fh, $config) = @_;
   my %general_configuration;
   for my $name (
      qw( workdir cleanup bundle deploy
      gzip bzip2 passthrough tempdir-mode )
     )
   {
      $general_configuration{$name} = $config->{$name}
        if exists $config->{$name};
   } ## end for my $name (qw( workdir cleanup bundle deploy...))
   my $configuration = Dumper \%general_configuration;
   print {$fh} header(name => 'config.pl', size => length($configuration)),
     "\n", $configuration, "\n\n";
} ## end sub print_configuration

# Process files and directories. All these will be reported in the
# extraction directory, i.e. basename() will be applied to them. For
# directories, they will be re-created
sub print_here_stuff {
   my $fh     = shift;
   my $config = shift;
   my @ARGV   = @_;

   my $ai = Deployable::Tar->new($config);
   $ai->add(
      '.' => \@ARGV,
      '.' => $config->{herefile},
      map { $_ => ['.'] } @{$config->{heredir}}
   );

   print {$fh} header(name => 'here', size => $ai->size()), "\n";
   $ai->copy_to($fh);
   print {$fh} "\n\n";

   return;
} ## end sub print_here_stuff

sub print_root_stuff {
   my ($fh, $config) = @_;

   my $ai = Deployable::Tar->new($config);
   $ai->add(
      '.' => $config->{rootdir},
      '.' => $config->{rootfile},
      (undef, $config->{tarfile}),
      map { $_ => ['.'] } @{$config->{root}},
   );

   print {$fh} header(name => 'root', size => $ai->size()), "\n";
   $ai->copy_to($fh);
   print {$fh} "\n\n";

   return;
} ## end sub print_root_stuff

sub get_remote_script {
   my $fh;
   if (-e $config{remote}) {
      open $fh, '<', $config{remote}
        or croak "open('$config{remote}'): $OS_ERROR";
   }
   else {
      no warnings 'once';
      $fh = \*DATA;
   }
   my @lines;
   while (<$fh>) {
      last if /\A __END__ \s*\z/mxs;
      push @lines, $_;
   }
   close $fh;
   return join '', @lines, "__END__\n";
} ## end sub get_remote_script

package Deployable::Tar;

sub new {
   my $package = shift;
   my $self = {ref $_[0] ? %{$_[0]} : @_};
   $package = 'Deployable::Tar::Internal';
   if (!$self->{'no-tar'}) {
      if ((exists $self->{tar}) || (open my $fh, '-|', 'tar', '--help')) {
         $package = 'Deployable::Tar::External';
         $self->{tar} ||= 'tar';
      }
   } ## end if (!$self->{'no-tar'})
   bless $self, $package;
   $self->initialise();
   return $self;
} ## end sub new

package Deployable::Tar::External;
use File::Temp qw( :seekable );
use English qw( -no_match_vars );
use Cwd ();
use Carp;
our @ISA = qw( Deployable::Tar );

sub initialise {
   my $self = shift;
   $self->{_temp}     = File::Temp->new();
   $self->{_filename} = Cwd::abs_path($self->{_temp}->filename());
   return $self;
} ## end sub initialise

sub add {
   my $self = shift;
   my $tar  = $self->{tar};
   delete $self->{_compressed};
   while (@_) {
      my ($directory, $stuff) = splice @_, 0, 2;
      my @stuff = @$stuff;
      if (defined $directory) {
         while (@stuff) {
            my @chunk = splice @stuff, 0, 50;
            system {$tar} $tar, 'rvf', $self->{_filename},
              '-C', $directory, '--', @chunk;
         }
      } ## end if (defined $directory)
      else {    # it's another TAR file, concatenate
         while (@stuff) {
            my @chunk = splice @stuff, 0, 50;
            system {$tar} $tar, 'Avf', $self->{_filename}, '--', @chunk;
         }
      } ## end else [ if (defined $directory)]
   } ## end while (@_)
   return $self;
} ## end sub add

sub _compress {
   my $self = shift;
   return if exists $self->{_compressed};

   $self->{_temp}->sysseek(0, SEEK_SET);
   if ($self->{bzip2}) {
      require IO::Compress::Bzip2;
      $self->{_compressed} = File::Temp->new();

      # double-quotes needed to force usage of filename
      # instead of filehandle
      IO::Compress::Bzip2::bzip2($self->{_temp}, "$self->{_compressed}");
   } ## end if ($self->{bzip2})
   elsif ($self->{gzip}) {
      require IO::Compress::Gzip;
      $self->{_compressed} = File::Temp->new();

      # double-quotes needed to force usage of filename
      # instead of filehandle
      IO::Compress::Gzip::gzip($self->{_temp}, "$self->{_compressed}");
   } ## end elsif ($self->{gzip})
   else {
      $self->{_compressed} = $self->{_temp};
   }

   return $self;
} ## end sub _compress

sub size {
   my ($self) = @_;
   $self->_compress();
   return (stat $self->{_compressed})[7];
}

sub copy_to {
   my ($self, $out_fh) = @_;
   $self->_compress();
   my $in_fh = $self->{_compressed};
   $in_fh->sysseek(0, SEEK_SET);
   while ('true') {
      my $nread = $in_fh->sysread(my $buffer, 4096);
      croak "sysread(): $OS_ERROR" unless defined $nread;
      last unless $nread;
      print {$out_fh} $buffer;
   } ## end while ('true')
   return $self;
} ## end sub copy_to

package Deployable::Tar::Internal;
use Archive::Tar     ();
use Cwd              ();
use File::Find::Rule ();
use Carp qw< croak >;
our @ISA = qw( Deployable::Tar );

sub initialise {
   my $self = shift;
   $self->{_tar} = Archive::Tar->new();
   return $self;
}

sub add {
   my $self = shift;
   delete $self->{_string};
   my $tar = $self->{_tar};
   my $cwd = Cwd::getcwd();
   while (@_) {
      my ($directory, $stuff) = splice @_, 0, 2;
      if (defined $directory) {
         chdir $directory;
         for my $item (@$stuff) {
            if (ref $item) {
               my ($src, $filename) = @$item;
               my $src_len = length $src;
               for my $input (File::Find::Rule->in($src)) {
                  my ($atf) = $tar->add_files($input);
                  my $name = $filename . substr $input, $src_len;
                  $atf->rename($name);
               }
            }
            else {
               $tar->add_files($_) for File::Find::Rule->in($item);
            }
         }
         chdir $cwd;
      } ## end if (defined $directory)
      else { # It's another TAR file to be concatenated
         for my $item (@$stuff) {
            my $iterator = Archive::Tar->iter($item);
            while (my $f = $iterator->()) {
               $tar->add_files($f);
            }
         }
      }
   } ## end while (@_)
   return $self;
} ## end sub add

sub size {
   my ($self) = @_;
   $self->{_string} = $self->{_tar}->write()
     unless exists $self->{_string};
   return length $self->{_string};
} ## end sub size

sub copy_to {
   my ($self, $out_fh) = @_;
   $self->{_string} = $self->{_tar}->write()
     unless exists $self->{_string};
   print {$out_fh} $self->{_string};
} ## end sub copy_to

=head1 NAME

deployable - create a deploy script for some files/scripts

=head1 VERSION

See version at beginning of script, variable $VERSION, or call

   shell$ deployable --version

=head1 USAGE

   deployable [--usage] [--help] [--man] [--version]

   deployable [--bundle|--all-exec|-X]
              [--bzip2|--bz2|-j]
              [--cleanup|-c]
              [--deploy|--exec|d <program>]
              [--gzip|-gz|-z]
              [--heredir|-H <dirname>]
              [--include-archive-tar|-T]
              [--no-tar]
              [--output|-o <filename>]
              [--root|-r <dirname>]
              [--rootdir|--in-root|-R <dirname>]
              [--tar|-t <program-path>]
              [--tarfile|-F <filename>]
              [--tempdir-mode|-m <mode>]
              [--xform|-x src:filename]
              [--workdir|-w <path>]
              [ files or directories... ]

=head1 EXAMPLES

   # pack some files and a deploy script together.
   shell$ deployable script.sh file.txt some/directory -d script.sh

   # Use a directory's contents as elements for the target root
   shell$ ls -1 /path/to/target/root
   etc
   opt
   usr
   var
   # The above will be deployed as /etc, /opt, /usr and /var
   shell$ deployable -o dep.pl --root /path/to/target/root

   # Include sub-directory etc/ for inclusion and extraction
   # directly as /etc/
   shell$ deployable -o dep.pl --in-root etc/

=head1 DESCRIPTION

This is a meta-script to create deploy scripts. The latter ones are
suitable to be distributed in order to deploy something.

You basically have to provide two things: files to install and programs
to be executed. Files can be put directly into the deployed script, or
can be included in gzipped tar archives.

When called, this script creates a deploy script for you. This script
includes all the specified files, and when executed it will extract
those files and execute the given programs. In this way, you can ship
both files and logic needed to correctly install those files, but this
is of course of of scope.

All files and archives will be extracted under a configured path
(see L<--workdir> below), which we'll call I<workdir> from now on. Under
the I<workdir> a temporary directory will be created, and the files
will be put in the temporary directory. You can specify if you want to
clean up this temporary directory or keep it, at your choice. (You're able
to both set a default for this cleanup when invoking deployable, or when
invoking the deploy script itself). The temporary directory will be
called I<tmpdir> in the following.

There are several ways to embed files to be shipped:

=over

=item *

pass the name of an already-prepared tar file via L</--tarfile>. The
contents of this file will be assumed to be referred to the root
directory;

=item *

specify the file name directly on the command line. A file given in this
way will always be extracted into the I<tmpdir>, whatever its initial path
was;

=item *

specify the name of a directory on the command line. In this case,
C<tar> will be used to archive the directory, with the usual option to
turn absolute paths into relative ones; this means that directories will
be re-created under I<tmpdir> when extraction is performed;

=item *

give the name of a directory to be used as a "here directory", using
the C<--heredir|-H> option. This is much the same as giving the directory
name (see above), but in this case C<tar> will be told to change into the
directory first, and archive '.'. This means that the contents of the
"here-directory" will be extracted directly into I<tmpdir>.

=back

=head2 Extended Example

Suppose you have a few server which have the same configuration, apart
from some specific stuff (e.g. the hostname, the IP addresses, etc.).
You'd like to perform changes to all with the minimum work possible...
so you know you should script something.

For example, suppose you want to update a few files in /etc, setting these
files equal for all hosts. You would typically do the following:

   # In your computer
   shell$ mkdir -p /tmp/newfiles/etc
   shell$ cd /tmp/newfiles/etc
   # Craft the new files
   shell$ cd ..
   shell$ tar cvzf newetc.tar.gz etc

   # Now, for each server:
   shell$ scp newetc.tar.gz $server:/tmp
   shell$ ssh $server tar xvzf /tmp/newetc.tar.gz -C /


So far, so good. But what if you need to kick in a little more logic?
For example, if you update some configuration files, you'll most likey
want to restart some services. So you could do the following:

   shell$ mkdir -p /tmp/newfiles/tmp
   shell$ cd /tmp/newfiles/tmp
   # craft a shell script to be executed remotely and set the exec bit
   # Suppose it's called deploy.sh
   shell$ cd ..
   shell$ tar cvzf newetc.tar.gz etc tmp

   # Now, for each server:
   shell$ scp newetc.tar.gz $server:/tmp
   shell$ ssh $server tar xvzf /tmp/newetc.tar.gz -C /
   shell$ ssh $server /tmp/deploy.sh

And what if you want to install files depending on the particular machine?
Or you have a bundle of stuff to deploy and a bunch of scripts to execute?
You can use deployable. In this case, you can do the following:

   shell$ mkdir -p /tmp/newfiles/etc
   shell$ cd /tmp/newfiles/etc
   # Craft the new files
   shell$ cd ..
   # craft a shell script to be executed remotely and set the exec bit
   # Suppose it's called deploy.sh
   shell$ deployable -o deploy.pl -R etc deploy.sh -d deploy.sh

   # Now, for each server
   shell$ scp deploy.pl $server:/tmp
   shell$ ssh $server /tmp/deploy.pl

And you're done. This can be particularly useful if you have another
layer of deployment, e.g. if you have to run a script to decide which
of a group of archives should be deployed. For example, you could craft
a different new "etc" for each server (which is particularly true if
network configurations are in the package), and produce a simple script
to choose which file to use based on the MAC address of the machine. In
this case you could have:

=over

=item newetc.*.tar.gz

a bunch of tar files with the configurations for each different server

=item newetc.list

a list file with the association between the MAC addresses and the
real tar file to deploy from the bunch in the previous bullet

=item deploy-the-right-stuff.sh

a script to get the real MAC address of the machine, select the right
tar file and do the deployment.

=back

So, you can do the following:

   shell$ deployable -o deploy.pl newetc.*.tar.gz newetc.list \
      deploy-the-right-stuff.sh --exec deploy-the-right-stuff.sh

   # Now, for each server:
   shell$ scp deploy.pl $server:/tmp
   shell$ ssh $server /tmp/deploy.pl

So, once you have the deploy script on the target machine all you need
to do is to execute it. This can come handy when you cannot access the
machines from the network, but you have to go there physically: you
can prepare all in advance, and just call the deploy script.


=head1 OPTIONS

Meta-options:

=over

=item B<--help>

print a somewhat more verbose help, showing usage, this description of
the options and some examples from the synopsis.

=item B<--man>

print out the full documentation for the script.

=item B<--usage>

print a concise usage line and exit.

=item B<--version>

print the version of the script.

=back

Real-world options:

=over

=item B<< --bundle | --all-exec | -X >>

Set bundle flag in the produced script. If the bundle flag is set, the
I<deploy script> will treat all executables in the main deployment
directory as scripts to be executed.

By default the flag is not set.

=item B<< --bzip2 | --bz2 | -j >>

Compress tar archives with bzip2.

=item B<< --cleanup | -c >>

Set cleanup flag in the produced script. If the cleanup flag is set, the
I<deploy script> will clean up after having performed all operations.

You can set this flag to C<0> by using C<--no-cleanup>.

=item B<< --deploy | --exec | -d <filename> >>

Set the name of a program to execute after extraction. You can provide
multiple program names, they will be executed in the same order.

=item B<< --gzip | --gz | -z >>

Compress tar archives with gzip.

=item B<< --heredir | -H <path> >>

Set the name of a "here directory" (see L<DESCRIPTION>). You can use this
option multiple times to provide multiple directories.

=item B<< --include-archive-tar | -T >>

Embed L<Archive::Tar> (with its dependencies L<Archive::Tar::Constant> and
L<Archive::Tar::File>) inside the final script. Use this when you know (or
aren't sure) that L<Archive::Tar> will not be available in the target
machine.

=item B<< --no-tar >>

Don't use system C<tar>.

=item B<< --output | -o <filename> >>

Set the output file name. By default the I<deploy script> will be given
out on the standard output; if you provide a filename (different from
C<->, of course!) the script will be saved there and the permissions will
be set to 0755.

=item B<< --root | -r <dirname> >>

Include C<dirname> contents for deployment under root directory. The
actual production procedure is: hop into C<dirname> and grab a tarball
of C<.>. During deployment, hop into C</> and extract the tarball.

This is useful if you're already building up the absolute deployment
layout under a given directory: just treat that directory as if it were
the root of the target system.

=item B<< --rootdir | --in-root | -R <filename> >>

Include C<filename> as an item that will be extracted under root
directory. The actual production procedure is: grab a tarball of
C<filename>. During deployment, hop into C</> and extract the tarball.

This is useful e.g. if you have a directory (or a group of directories)
that you want to deploy directly under the root.

Note that the C<--rootdir> alias is kept for backwards compatibility
but is not 100% correct - you can specify both a dirname (like it was
previously stated) or a single file with this option. This is why it's
more readably to use C<--in-root> instead.

=item B<< --tar | -t <program-path> >>

Set the system C<tar> program to use.

=item B<< --tempdir-mode | -m >>

set default permissions for temporary directory of deployable script

=item B<< --workdir | --deploy-directory | -w <path> >>

Set the working directory for the deploy.

=back

=head1 ROOT OR ROOTDIR?

There are two options that allow you to specify things to be deployed
in C</>, so what should you use? Thing is... whatever you want!

If you have a bunch of directories that have to appear under root, probably
your best bet is to put them all inside a directory called C<myroot> and
use option C<--root>:

   shell$ mkdir -p myroot/{etc,opt,var,lib,usr,whatever}
   # Now put stuff in the directories created above...
   shell$ deployable --root myroot ...

On the other hand, if you just want to put stuff starting from one or
two directories that have to show up in C</>, you can avoid creating
the extra C<myroot> directory and use C<--in-root> instead:

   shell$ mkdir -p etc/whatever
   # Now put stuff in etc/whatever...
   shell$ deployable --in-root etc ...

They are indeed somehow equivalent, the first avoiding you much typing
when you have many directories to be deployed starting from root (just
put them into the same subdirectory), the second allowing you to avoid
putting an extra directory layer.

There is indeed an additional catch that makes them quite different. When
you use C<root>, the whole content of the directory specified will be
used as a base, so you will end up with a listing like this:

   opt/
   opt/local/
   opt/local/application/
   opt/local/application/myfile.txt
   opt/local/application/otherfile.txt

i.e. all intermediate directories will be saved. On the other hand, when
you specify a directory with C<--in-root>, you're not limited to provide
a "single-step" directory, so for example:

   shell$ deployable --in-root opt/local/application

will result in the following list of files/directories to be stored:

   opt/local/application/
   opt/local/application/myfile.txt
   opt/local/application/otherfile.txt

i.e. the upper level directories will not be included. What is better for
you is for you to judge.

=head1 THE DEPLOY SCRIPT

The net result of calling this script is to produce another script,
that we call the I<deploy script>. This script is made of two parts: the
code, which is fixed, and the configurations/files, which is what is
actually produced. The latter part is put after the C<__END__> marker,
as usual.

Stuff in the configuration part is always hexified in order to prevent
strange tricks or errors. Comments will help you devise what's inside the
configurations themselves.

The I<deploy script> has options itself, even if they are quite minimal.
In particular, it supports the same options C<--workdir|-w> and
C<--cleanup> described above, allowing the final user to override the
configured values. By default, the I<workdir> is set to C</tmp>
and the script will clean up after itself.

The following options are supported in the I<deploy script>:

=over

=item B<--usage | --man | --help>

print a minimal help and exit

=item B<--version>

print script version and exit

=item B<--bundle | --all-exec | -X>

treat all executables in the main deployment directory as scripts
to be executed

=item B<--cleanup | --no-cleanup>

perform / don't perform temporary directory cleanup after work done

=item B<< --deploy | --no-deploy >>

deploy scripts are executed by default (same as specifying '--deploy')
but you can prevent it.

=item B<--dryrun | --dry-run>

print final options and exit

=item B<< --filelist | --list | -l >>

print a list of files that are shipped in the deploy script

=item B<< --heretar | --here-tar | -H >>

print out the tar file that contains all the files that would be
extracted in the temporary directory, useful to redirect to file or
pipe to the tar program

=item B<< --inspect <dirname> >>

just extract all the stuff into <dirname> for inspection. Implies
C<--no-deploy>, C<--no-tempdir>, ignores C<--bundle> (as a consequence of
C<--no-deploy>), disables C<--cleanup> and sets the working directory
to C<dirname>

=item B<< --no-tar >>

don't use system C<tar>

=item B<< --rootar | --root-tar | -R >>

print out the tar file that contains all the files that would be
extracted in the root directory, useful to redirect to file or
pipe to the tar program

=item B<--show | --show-options | -s>

print configured options and exit

=item B<< --tar | -t <program-path> >>

set the system C<tar> program to use.

=item B<< --tarfile | -F <filename> >>

add the specified C<filename> (assumed to be an uncompressed
TAR file) to the lot for root extraction. This can come handy
when you already have all the files backed up in a TAR archive
and you're not willing to expand them (e.g. because your
filesystem is case-insensitive...).

=item B<< --tempdir | --no-tempdir >>

by default a temporary directory is created (same as specifying
C<--tempdir>), but you can execute directly in the workdir (see below)
without creating it.

=item B<< --tempdir-mode | -m >>

temporary directories (see C<--tempdir>) created by File::Temp have
permission 600 that prevents group/others from even looking at the
contents. You might want to invoke some of the internal scripts
from another user (e.g. via C<su>), so you can pass a mode to be
set on the temporary directory.

Works only if C<--tempdir> is active.

=item B<< --xform | -x <src:filename> >>

include file or directory C<src> as path C<filename>. The latter can be
either an absolute path (included in C<root>) or a relative one
(included in the working directory).

=item B<--workdir | --work-directory | --deploy-directory | -w>

working base directory (a temporary subdirectory will be created
there anyway)

=back

Note the difference between C<--show> and C<--dryrun>: the former will
give you the options that are "embedded" in the I<deploy script> without
taking into account other options given on the command line, while the
latter will give you the final options that would be used if the script
were called without C<--dryrun>.

=head2 Deploy Script Example Usage

In the following, we'll assume that the I<deploy script> is called
C<deploy.pl>.

To execute the script with the already configured options, you just have
to call it:

   shell$ ./deploy.pl

If you just want to see which configurations are in the I<deploy script>:

   shell$ ./deploy.pl --show

To see which files are included, you have two options. One is asking the
script:

   shell$ ./deploy.pl --filelist

the other is piping to tar:

   shell$ ./deploy.pl --tar | tar tvf -

Extract contents of the script in a temp directory and simply inspect
what's inside:

   # extract stuff into subdirectory 'inspect' for... inspection
   shell$ ./deploy.pl --no-tempdir --no-deploy --workdir inspect

=head2 Deploy Script Requirements

You'll need a working Perl with version at least 5.6.2.

If you specify L</--include-archive-tar>, the module L<Archive::Tar> will
be included as well. This should ease your life and avoid you to have
B<tar> on the target machine. On the other hand, if you already know
that B<tar> will be available, you can avoid including C<Archive::Tar>
and have the generated script use it (it could be rather slower anyway).

=head1 DIAGNOSTICS

Each error message should be enough explicit to be understood without the
need for furter explainations. Which is another way to say that I'm way
too lazy to list all possible ways that this script has to fail.


=head1 CONFIGURATION AND ENVIRONMENT

deployable requires no configuration files or environment variables.

Please note that deployable B<needs> to find its master B<remote> file
to produce the final script. This must be put in the same directory where
deployable is put. You should be able to B<symlink> deployable where you
think it's better, anyway - it will go search for the original file
and look for B<remote> inside the same directory. This does not apply to
hard links, of course.


=head1 DEPENDENCIES

All core modules, apart the following:

=over

=item B<< Archive::Tar >>

=item B<< File::Find::Rule >>

=back

=head1 BUGS AND LIMITATIONS

No bugs have been reported.

Please report any bugs or feature requests to the AUTHOR below.

Be sure to read L<CONFIGURATION AND ENVIRONMENT> for a slight limitation
about the availability of the B<remote> script.

=head1 AUTHOR

Flavio Poletti C<flavio [AT] polettix.it>


=head1 LICENSE AND COPYRIGHT

Copyright (c) 2008, Flavio Poletti C<flavio [AT] polettix.it>. All rights reserved.

This script is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>
and L<perlgpl>.

=head1 DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENCE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

=cut

package main; # ensure DATA is main::DATA
__DATA__
